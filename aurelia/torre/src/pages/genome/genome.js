import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { Repository } from '../../services/repository';
import endpoints from '../../endpoints';
import userData from '../../user-data';

@inject(Repository)
export class Genome {
    constructor(repository) {
        this.repository = repository;
        this.personalGenome = false;
    }

    async activate(params) {
        var username = 'kudachinyemba';

        if (params && params.username) {
            this.username = params.username;
            this.personalGenome = false;
        } else {
            this.personalGenome = true;
        }

        //not used because of cors errors
        // this.user = await this.repository.GetAsync(`${endpoints.torreBio}/api/bios/${username}`, true);

        this.userData = userData;
    }
}