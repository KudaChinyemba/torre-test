import { inject, bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { Repository } from '../../services/repository';
import endpoints from '../../endpoints';

@inject(Repository)
export class Search {

    @bindable page = 1;

    constructor(repository) {
        this.repository = repository;

        this.size = 20;
        this.aggregators = {};
        this.module = 'people'; //people, jobs
    }

    get offset() {
        return (this.page - 1) * this.size;
    }

    get isPerson() {
        return this.module == 'people';
    }

    get canGoNext() {
        return this.items && this.result ? this.offset + this.items.length < this.result.total : false;
    }

    get canGoPrev() {
        return this.page > 1;
    }

    pageChanged() {
        if (!this.skipReload)
            return this.searchAsync();
    }

    setModule = (param) => {
        this.module = param;
        this.clearFilters();
        this.searchAsync();
    }

    clearFilters = () => {
        this.aggregators = {};
        this.skipReload = true;
        this.page = 1;
    }

    async activate() {
        return await this.searchAsync();
    }

    async searchAsync() {
        this.loading = true;

        this.items = [];
        this.result = null;

        let searchModule = this.module == 'people' ? 'people' : 'opportunities';

        this.result = await this.repository.SendAsync(`${endpoints.torreSearch}/${searchModule}/_search/?offset=${this.offset}&size=${this.size}&aggregate=`, null, 'post', true);

        if (this.result) {
            this.items = this.result.results;
        }

        this.skipReload = false;
        this.loading = false;
    }

    next = () => {
        if (this.canGoNext)
            this.page++;
    }

    prev = () => {
        if (this.canGoPrev)
            this.page--;
    }
}