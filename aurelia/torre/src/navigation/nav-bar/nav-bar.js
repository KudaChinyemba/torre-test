import {
    inject
} from 'aurelia-framework';
import {
    Router
} from 'aurelia-router';
import { Repository } from '../../services/repository';
import endpoints from '../../endpoints';

@inject(Router, Repository)
export class NavBar {
    constructor(router, repository) {
        this.router = router;
        this.repository = repository;
    }

    get currentRouteSettings() {
        return this.router.currentInstruction.config.settings;
    }

}