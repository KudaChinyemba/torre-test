export default {
    torreRoot: 'https://torre.co',
    torreBio: 'https://torre.bio',
    torreSearch: 'https://search.torre.co'
}