import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { Repository } from './services/repository';
import endpoints from './endpoints';
import { PLATFORM } from "aurelia-framework";

@inject(Repository)
export class App {
    message = 'Hello World!';

    constructor(repository) {
        this.repository = repository;
    }

    configureRouter(config, router) {
        this.router = router;
        config.options.pushState = true;
        config.options.root = '/';

        config.map([{
                route: ['', 'search'],
                name: 'search',
                moduleId: PLATFORM.moduleName('./pages/search/search'),
                nav: true,
                title: 'Search',
                settings: {
                    iconClass: 'fas fa-search'
                }
            },
            {
                route: 'genome/:username',
                name: 'genome',
                moduleId: PLATFORM.moduleName('./pages/genome/genome'),
                nav: false,
                title: 'Genome',
                settings: {
                    iconClass: 'fas fa-user'
                }
            },
            {
                route: 'your-genome',
                name: 'your-genome',
                moduleId: PLATFORM.moduleName('./pages/genome/genome'),
                nav: true,
                title: 'Genome',
                settings: {
                    iconClass: 'fas fa-user'
                }
            }
        ]);

    }
}