export default {
    "person": {
        "professionalHeadline": "Technical director at Byteden software engineering",
        "completion": 0.75,
        "showPhone": false,
        "created": "2020-07-27T20:19:12Z",
        "verified": true,
        "flags": {
            "benefits": true,
            "canary": false,
            "enlauSource": false,
            "fake": false,
            "featureDiscovery": true,
            "getSignaledBenefitsViewed": false,
            "firstSignalSent": true,
            "promoteYourselfBenefitsViewed": false,
            "promoteYourselfCompleted": false,
            "importingLinkedin": false,
            "onBoarded": true,
            "remoter": true,
            "signalsFeatureDiscovery": true,
            "signedInToOpportunities": true,
            "importingLinkedinRecommendations": true,
            "contactsImported": true,
            "appContactsImported": false,
            "genomeCompletionAcknowledged": false
        },
        "weight": 0,
        "locale": "en",
        "subjectId": 577221,
        "picture": "https://starrgate.s3.amazonaws.com:443/users/6be34d8fe95413c71c39efcf1aaf2bde11b85091/profile_BwcfWoO.jpg",
        "hasEmail": true,
        "name": "Kudakwashe Ronald Chinyemba",
        "links": [{
                "id": "KNx6mprj",
                "name": "",
                "address": "https://www.byteden.co.zw"
            },
            {
                "id": "GNKno4oy",
                "name": "linkedin",
                "address": "https://www.linkedin.com/in/kudakwashe-ronald-chinyemba-66aa812a/"
            }
        ],
        "location": {
            "name": "Harare, Zimbabwe",
            "shortName": "Zimbabwe",
            "country": "Zimbabwe",
            "latitude": -19.015438,
            "longitude": 29.154857,
            "timezone": "Africa/Harare",
            "timezoneOffSet": 7200000
        },
        "theme": "blue500",
        "id": "ny169R7N",
        "pictureThumbnail": "https://starrgate.s3.amazonaws.com:443/CACHE/images/users/6be34d8fe95413c71c39efcf1aaf2bde11b85091/profile_BwcfWoO/bf6286003cac8a16d7ad9ff82ff6b4e5.jpg",
        "claimant": false,
        "weightGraph": "https://hcti.io/v1/image/1916fb86-6265-4733-b7c8-ec175a0164e8",
        "publicId": "kudachinyemba"
    },
    "stats": {
        "strengths": 28,
        "awards": 1,
        "education": 1,
        "interests": 3,
        "jobs": 4,
        "projects": 1
    },
    "strengths": [{
            "id": "5ykgvBPy",
            "code": 59925,
            "name": "Node",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:50:26"
        },
        {
            "id": "ZNOB8YDy",
            "code": 55799,
            "name": "MySQL",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:50:06"
        },
        {
            "id": "kybZa8gy",
            "code": 23802,
            "name": "HTML",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:49:44"
        },
        {
            "id": "7M2zdG6M",
            "code": 56083,
            "name": "Redis",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:49:18"
        },
        {
            "id": "qy0koZmj",
            "code": 59162,
            "name": "SQL",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:49:00"
        },
        {
            "id": "AMWbxZxN",
            "code": 32018,
            "name": "C#",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:48:28"
        },
        {
            "id": "xyXbK3mM",
            "code": 26687,
            "name": "Flutter",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:48:11"
        },
        {
            "id": "LMgpmlLj",
            "code": 46705,
            "name": "Mobile Development",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:47:52"
        },
        {
            "id": "EM3Wo7bM",
            "code": 25720,
            "name": "Web Development",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:47:27"
        },
        {
            "id": "vN8eoGQM",
            "code": 55129,
            "name": ".net core",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:47:05"
        },
        {
            "id": "xyXbK3eM",
            "code": 18319,
            "name": "Javascript",
            "additionalInfo": "",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-27T16:46:49"
        },
        {
            "id": "zNG1pp9j",
            "code": 22886,
            "name": "Quality management",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:16:37"
        },
        {
            "id": "qNP1nnBM",
            "code": 55437,
            "name": "Efficiency",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:16:28"
        },
        {
            "id": "9ME1vv2M",
            "code": 55317,
            "name": "Creativity",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:09:54"
        },
        {
            "id": "VjwnrrrM",
            "code": 56096,
            "name": "Resourceful",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:09:48"
        },
        {
            "id": "GNK9nnAj",
            "code": 18645,
            "name": "Marketing",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:09:38"
        },
        {
            "id": "KNxk6zQy",
            "code": 55266,
            "name": "Communication",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:09:32"
        },
        {
            "id": "gyYV3oON",
            "code": 40451,
            "name": "Customer Service",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:09:16"
        },
        {
            "id": "5ykgzray",
            "code": 55795,
            "name": "Management",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T23:01:30"
        },
        {
            "id": "AjrGDpWy",
            "code": 55811,
            "name": "Responsible",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:58:35"
        },
        {
            "id": "ANab2vOM",
            "code": 45396,
            "name": "Leadership",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:58:10"
        },
        {
            "id": "BML1nXAj",
            "code": 6217,
            "name": "Project Management",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:56:16"
        },
        {
            "id": "gyYV3rDN",
            "code": 521,
            "name": "Engineering",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:55:55"
        },
        {
            "id": "kybZd11y",
            "code": 704,
            "name": "Software Development",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:50:08"
        },
        {
            "id": "pyJpqZEM",
            "code": 56184,
            "name": "Strategic Thinking",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-07-27T20:28:07"
        },
        {
            "id": "LMgGZQmM",
            "code": 60546,
            "name": "Analytical",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-07-27T20:28:07"
        },
        {
            "id": "xyXxeDpy",
            "code": 56300,
            "name": "Critical",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-07-27T20:28:07"
        },
        {
            "id": "7M2zVDAM",
            "code": 52274,
            "name": "Software engineering",
            "weight": 0,
            "recommendations": 0,
            "media": [

            ],
            "created": "2020-08-11T22:48:21"
        }
    ],
    "interests": [{
            "id": "WNV1w5vy",
            "code": 40852,
            "name": "DevOps",
            "media": [

            ],
            "created": "2020-07-27T20:28:56"
        },
        {
            "id": "9MB56g8N",
            "code": 33351,
            "name": "Automation",
            "media": [

            ],
            "created": "2020-07-27T20:28:56"
        },
        {
            "id": "kybewwRj",
            "code": 61332,
            "name": "Software development",
            "media": [

            ],
            "created": "2020-08-28T21:12:24"
        }
    ],
    "experiences": [{
            "id": "WNVJ55qN",
            "category": "education",
            "name": "B.Eng Civil (structural engineering and engineering informatics)",
            "organizations": [{
                "id": 406878,
                "name": "Stellenbosch University/Universiteit Stellenbosch",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1599867359/origin/bio/organizations/w2xzrbi9gqec07jsfbmh.png"
            }],
            "responsibilities": [

            ],
            "fromMonth": "January",
            "fromYear": "2011",
            "toMonth": "November",
            "toYear": "2015",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 1
        },
        {
            "id": "7M2moDgN",
            "category": "projects",
            "name": "Multiple projects",
            "organizations": [{
                    "id": 521476,
                    "name": "Khanyisa Real Systems",
                    "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
                },
                {
                    "id": 461814,
                    "name": "Dimension Data South Africa",
                    "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1602552321/origin/bio/organizations/ysui30zjjzboqk1ogwti.png"
                },
                {
                    "id": 490913,
                    "name": "Getworth",
                    "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603769676/origin/bio/organizations/la2eocr0ric9apa6tqbr.png"
                },
                {
                    "id": 491938,
                    "name": "Byteden software engineering",
                    "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603820318/origin/bio/organizations/al8uzerjhon3vskshbxc.png"
                },
                {
                    "id": 406878,
                    "name": "Stellenbosch University/Universiteit Stellenbosch",
                    "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1599867359/origin/bio/organizations/w2xzrbi9gqec07jsfbmh.png"
                }
            ],
            "responsibilities": [
                "Have been a full-stack developer since I started and have worked on countless projects by now, using various programming languages, frameworks, tools and technologies. I have vast experiences across board from back-end to front-end technologies."
            ],
            "fromMonth": "January",
            "fromYear": "2016",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 1
        },
        {
            "id": "vy5OBBXM",
            "category": "awards",
            "name": "Top junior developer",
            "organizations": [{
                "id": 521476,
                "name": "Khanyisa Real Systems",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
            }],
            "responsibilities": [

            ],
            "fromMonth": "January",
            "fromYear": "2016",
            "toMonth": "June",
            "toYear": "2017",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 1
        },
        {
            "id": "gyYmbv0y",
            "category": "jobs",
            "name": "Founder and director",
            "organizations": [{
                "id": 491938,
                "name": "Byteden software engineering",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603820318/origin/bio/organizations/al8uzerjhon3vskshbxc.png"
            }],
            "responsibilities": [
                "Running and maintaining my own consultancy. It's been a journey in which I have learnt a lot and much of it I wouldn't have had I not run my own business. I run a remote team and guide them in designing, implementing and supporting software."
            ],
            "fromMonth": "May",
            "fromYear": "2018",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 1
        },
        {
            "id": "xyX3rrrj",
            "category": "jobs",
            "name": "Tech lead",
            "organizations": [{
                "id": 490913,
                "name": "Getworth",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603769676/origin/bio/organizations/la2eocr0ric9apa6tqbr.png"
            }],
            "responsibilities": [
                "Led a small start-up team in designing, implementing, maintaining and expanding of the tools, services and products needed in a car dealership fin-tech start-up."
            ],
            "fromMonth": "October",
            "fromYear": "2017",
            "toMonth": "May",
            "toYear": "2018",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 2
        },
        {
            "id": "5yk6JJZj",
            "category": "jobs",
            "name": "Software engineer",
            "organizations": [{
                "id": 461814,
                "name": "Dimension Data South Africa",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1602552321/origin/bio/organizations/ysui30zjjzboqk1ogwti.png"
            }],
            "responsibilities": [
                "Was employed as a lead engineer and had to lead a team in delivering and supporting a number of projects."
            ],
            "fromMonth": "July",
            "fromYear": "2017",
            "toMonth": "October",
            "toYear": "2017",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 3
        },
        {
            "id": "mjlJRWaN",
            "category": "jobs",
            "name": "Software developer",
            "organizations": [{
                "id": 521476,
                "name": "Khanyisa Real Systems",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
            }],
            "responsibilities": [
                "Being an active team member, collaborate in designing and implementing custom software according to client specification."
            ],
            "fromMonth": "January",
            "fromYear": "2016",
            "toMonth": "June",
            "toYear": "2017",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 4
        }
    ],
    "awards": [{
        "id": "vy5OBBXM",
        "category": "awards",
        "name": "Top junior developer",
        "organizations": [{
            "id": 521476,
            "name": "Khanyisa Real Systems",
            "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
        }],
        "responsibilities": [

        ],
        "fromMonth": "January",
        "fromYear": "2016",
        "toMonth": "June",
        "toYear": "2017",
        "highlighted": true,
        "weight": 0,
        "verifications": 0,
        "recommendations": 0,
        "media": [

        ],
        "rank": 1
    }],
    "jobs": [{
            "id": "gyYmbv0y",
            "category": "jobs",
            "name": "Founder and director",
            "organizations": [{
                "id": 491938,
                "name": "Byteden software engineering",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603820318/origin/bio/organizations/al8uzerjhon3vskshbxc.png"
            }],
            "responsibilities": [
                "Running and maintaining my own consultancy. It's been a journey in which I have learnt a lot and much of it I wouldn't have had I not run my own business. I run a remote team and guide them in designing, implementing and supporting software."
            ],
            "fromMonth": "May",
            "fromYear": "2018",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 1
        },
        {
            "id": "xyX3rrrj",
            "category": "jobs",
            "name": "Tech lead",
            "organizations": [{
                "id": 490913,
                "name": "Getworth",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603769676/origin/bio/organizations/la2eocr0ric9apa6tqbr.png"
            }],
            "responsibilities": [
                "Led a small start-up team in designing, implementing, maintaining and expanding of the tools, services and products needed in a car dealership fin-tech start-up."
            ],
            "fromMonth": "October",
            "fromYear": "2017",
            "toMonth": "May",
            "toYear": "2018",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 2
        },
        {
            "id": "5yk6JJZj",
            "category": "jobs",
            "name": "Software engineer",
            "organizations": [{
                "id": 461814,
                "name": "Dimension Data South Africa",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1602552321/origin/bio/organizations/ysui30zjjzboqk1ogwti.png"
            }],
            "responsibilities": [
                "Was employed as a lead engineer and had to lead a team in delivering and supporting a number of projects."
            ],
            "fromMonth": "July",
            "fromYear": "2017",
            "toMonth": "October",
            "toYear": "2017",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 3
        },
        {
            "id": "mjlJRWaN",
            "category": "jobs",
            "name": "Software developer",
            "organizations": [{
                "id": 521476,
                "name": "Khanyisa Real Systems",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
            }],
            "responsibilities": [
                "Being an active team member, collaborate in designing and implementing custom software according to client specification."
            ],
            "fromMonth": "January",
            "fromYear": "2016",
            "toMonth": "June",
            "toYear": "2017",
            "highlighted": true,
            "weight": 0,
            "verifications": 0,
            "recommendations": 0,
            "media": [

            ],
            "rank": 4
        }
    ],
    "projects": [{
        "id": "7M2moDgN",
        "category": "projects",
        "name": "Multiple projects",
        "organizations": [{
                "id": 521476,
                "name": "Khanyisa Real Systems",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1605055492/origin/bio/organizations/yfhwgxtcd0labgtmiooy.png"
            },
            {
                "id": 461814,
                "name": "Dimension Data South Africa",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1602552321/origin/bio/organizations/ysui30zjjzboqk1ogwti.png"
            },
            {
                "id": 490913,
                "name": "Getworth",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603769676/origin/bio/organizations/la2eocr0ric9apa6tqbr.png"
            },
            {
                "id": 491938,
                "name": "Byteden software engineering",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1603820318/origin/bio/organizations/al8uzerjhon3vskshbxc.png"
            },
            {
                "id": 406878,
                "name": "Stellenbosch University/Universiteit Stellenbosch",
                "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1599867359/origin/bio/organizations/w2xzrbi9gqec07jsfbmh.png"
            }
        ],
        "responsibilities": [
            "Have been a full-stack developer since I started and have worked on countless projects by now, using various programming languages, frameworks, tools and technologies. I have vast experiences across board from back-end to front-end technologies."
        ],
        "fromMonth": "January",
        "fromYear": "2016",
        "highlighted": true,
        "weight": 0,
        "verifications": 0,
        "recommendations": 0,
        "media": [

        ],
        "rank": 1
    }],
    "publications": [

    ],
    "education": [{
        "id": "WNVJ55qN",
        "category": "education",
        "name": "B.Eng Civil (structural engineering and engineering informatics)",
        "organizations": [{
            "id": 406878,
            "name": "Stellenbosch University/Universiteit Stellenbosch",
            "picture": "https://res.cloudinary.com/torre-technologies-co/image/upload/v1599867359/origin/bio/organizations/w2xzrbi9gqec07jsfbmh.png"
        }],
        "responsibilities": [

        ],
        "fromMonth": "January",
        "fromYear": "2011",
        "toMonth": "November",
        "toYear": "2015",
        "highlighted": true,
        "weight": 0,
        "verifications": 0,
        "recommendations": 0,
        "media": [

        ],
        "rank": 1
    }],
    "opportunities": [{
            "id": "Mvb9DQRM",
            "interest": "jobs",
            "field": "active",
            "data": false
        },
        {
            "id": "Mqr0gPbj",
            "interest": "gigs",
            "field": "active",
            "data": true
        },
        {
            "id": "yJznBkmy",
            "interest": "gigs",
            "field": "private",
            "data": false
        },
        {
            "id": "N89OB62M",
            "interest": "gigs",
            "field": "desirable-compensation-currency",
            "data": "USD$"
        },
        {
            "id": "yz9w09zj",
            "interest": "gigs",
            "field": "desirable-compensation-amount",
            "data": 30
        },
        {
            "id": "MW0Gz0wj",
            "interest": "gigs",
            "field": "desirable-compensation-periodicity",
            "data": "hourly"
        },
        {
            "id": "yDgaBgaj",
            "interest": "gigs",
            "field": "desirable-compensation-publicly-visible",
            "data": true
        },
        {
            "id": "y0VlBVJM",
            "interest": "internships",
            "field": "active",
            "data": false
        },
        {
            "id": "Nx96G9KM",
            "interest": "mentoring",
            "field": "active",
            "data": true
        },
        {
            "id": "M7DYBDJj",
            "interest": "advising",
            "field": "active",
            "data": true
        },
        {
            "id": "yQXxvX3j",
            "interest": "industries",
            "field": "types",
            "data": [{
                    "code": 33426,
                    "locale": "en",
                    "name": "Software Engineering"
                },
                {
                    "code": 33341,
                    "locale": "en",
                    "name": "Information technology"
                },
                {
                    "code": 33335,
                    "locale": "en",
                    "name": "Software Development"
                }
            ]
        }
    ],
    "languages": [{
            "code": "en",
            "language": "English",
            "fluency": "fully-fluent"
        },
        {
            "code": "sn",
            "language": "Shona",
            "fluency": "fully-fluent"
        }
    ]
}