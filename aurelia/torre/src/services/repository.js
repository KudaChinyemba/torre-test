import {
    inject
} from 'aurelia-framework';
import endpoints from '../endpoints';
import {
    HttpClient as HttpFetch,
    json
} from 'aurelia-fetch-client';

@inject(HttpFetch)
export class Repository {

    constructor(httpFetch) {
        this.httpFetch = httpFetch;
        this.httpFetch.configure(config => {
            config
                .withDefaults({
                    headers: {
                        'X-Requested-With': 'Fetch'
                    }
                })
        });
    }

    async GetAsync(endpoint, openResponse) {
        try {
            var response = await this.httpFetch.fetch(`${endpoint}`);

            if (!response.ok) {
                //error occurred
                return null;
            }

            return openResponse ? await response.json() : response;
        } catch (error) {
            //handle error

            return null;
        }
    }

    async SendAsync(endpoint, item, action, openResponse) {
        try {
            var response = await this.httpFetch.fetch(`${endpoint}`, {
                method: action,
                body: item ? json(item) : null
            });

            if (!response.ok) {
                //error occurred
                return null;
            }

            return openResponse ? await response.json() : response;
        } catch (error) {
            //handle error

            return null;
        }
    }

}