import numeral from 'numeral';

export class PaginationDescriptionValueConverter {
    toView(value) {
        // offset: 0
        // size: 20
        // total: 907147

        return `Showing results ${numeral(value.offset + 1).format('0,0')} - ${numeral(value.offset + value.size).format('0,0')} of around ${numeral(value.total).format('0,0')}`;
    }
}