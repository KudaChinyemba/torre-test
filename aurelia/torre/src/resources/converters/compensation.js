import numeral from 'numeral';

export class CompensationValueConverter {
    toView(value) {
        // code: "range"
        // currency: "USD$"
        // maxAmount: 5000
        // minAmount: 1000
        // periodicity: "monthly"

        if (value.code != 'range') return '';

        var periodicity = '';

        switch (value.periodicity) {
            case 'monthly':
                periodicity = '/month';
                break;
            case 'weekly':
                periodicity = '/week';
                break;
            case 'daily':
                periodicity = '/day';
            case 'hourly':
                periodicity = '/hour';
                break;

            default:
                periodicity = '/year';
                break;
        }

        return `${value.currency} ${numeral(value.minAmount).format('0a')} - ${numeral(value.maxAmount).format('0a')}${periodicity}`;
    }
}