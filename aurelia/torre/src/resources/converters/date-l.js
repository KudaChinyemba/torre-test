import moment from 'moment';

export class DateLValueConverter {
    toView(value, format = 'ddd D MMM YYYY, h:mm A') {
        return moment.utc(value).local().format(format);
    }
}