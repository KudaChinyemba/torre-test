import { PLATFORM } from "aurelia-framework";

export function configure(config) {
    config.globalResources([PLATFORM.moduleName('./converters/date-l'),
        PLATFORM.moduleName('./converters/compensation'),
        PLATFORM.moduleName('./converters/pagination-description')
    ]);
}